let
  pkgs = import <nixpkgs> { config = {}; overrides = []; };
  os = "linux";
  build-triple = "x86_64-unknown-linux-gnu";
  target-triple = "x86_64-unknown-linux-gnu";
  channel = "nightly-2022-11-05";

  # this is stuff formatted in the BUILD file
  # ripped from `rules_nixpkgs/toolchains/rust/rust.bzl`
  binary_ext = "";
  dylib_ext = ".so";
  staticlib_ext = ".a";
  default_edition = "2021";
  stdlib_linkflags = ''["-lpthread", "-ldl"]'';
in
pkgs.buildEnv {
 extraOutputsToInstall = [ "out" "bin" "lib" ];
 name = "bazel-rust-toolchain";
 paths = [ (pkgs.rust-bin.fromRustupToolchain { channel = channel; }) ];
 postBuild = ''
  cat <<EOF > $out/BUILD
  filegroup(
      name = "rustc",
      srcs = ["bin/rustc"],
      visibility = ["//visibility:public"],
  )
  filegroup(
      name = "rustfmt",
      srcs = ["bin/rustfmt"],
      visibility = ["//visibility:public"],
  )
  filegroup(
      name = "cargo",
      srcs = ["bin/cargo"],
      visibility = ["//visibility:public"],
  )
  filegroup(
      name = "clippy_driver",
      srcs = ["bin/clippy-driver"],
      visibility = ["//visibility:public"],
  )
  filegroup(
      name = "rustc_lib",
      srcs = glob(
          [
              "bin/*.so",
              "lib/*.so",
              "lib/rustlib/*/codegen-backends/*.so",
              "lib/rustlib/*/codegen-backends/*.dylib",
              "lib/rustlib/*/bin/rust-lld",
              "lib/rustlib/*/lib/*.so",
              "lib/rustlib/*/lib/*.dylib",
          ],
          allow_empty = True,
      ),
      visibility = ["//visibility:public"],
  )
  load("@rules_rust//rust:toolchain.bzl", "rust_stdlib_filegroup")
  rust_stdlib_filegroup(
      name = "rust_std",
      srcs = glob(
          [
              "lib/rustlib/*/lib/*.rlib",
              "lib/rustlib/*/lib/*.so",
              "lib/rustlib/*/lib/*.dylib",
              "lib/rustlib/*/lib/*.a",
              "lib/rustlib/*/lib/self-contained/**",
          ],
          # Some patterns (e.g. `lib/*.a`) don't match anything, see https://github.com/bazelbuild/rules_rust/pull/245
          allow_empty = True,
      ),
      visibility = ["//visibility:public"],
  )
  filegroup(
      name = "rust_doc",
      srcs = ["bin/rustdoc"],
      visibility = ["//visibility:public"],
  )
  load('@rules_rust//rust:toolchain.bzl', 'rust_toolchain')
  rust_toolchain(
      name = "rust_nix_impl",
      rust_doc = ":rust_doc",
      rust_std = ":rust_std",
      rustc = ":rustc",
      rustfmt = ":rustfmt",
      cargo = ":cargo",
      clippy_driver = ":clippy_driver",
      rustc_lib = ":rustc_lib",
      binary_ext = "${binary_ext}",
      staticlib_ext = "${staticlib_ext}",
      dylib_ext = "${dylib_ext}",
      os = "${os}",
      exec_triple = "${build-triple}",
      target_triple = "${target-triple}",
      default_edition = "${default_edition}",
      stdlib_linkflags = ${stdlib_linkflags},
      visibility = ["//visibility:public"],
  )
  EOF
 '';
}
