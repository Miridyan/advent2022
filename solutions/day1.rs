enum Token {
    Break,
    Number(i32),
}

fn main() {
    let mut weights = include_str!("../input/day1.txt")
        .lines()
        .filter_map(|item| match item {
            "" => Some(Token::Break),
            n @ _ => i32::from_str_radix(n, 10)
                .ok()
                .map(|num| Token::Number(num)),
        })
        .fold(vec![0], |mut acc, item| match item {
            Token::Break => {
                acc.push(0);
                acc
            }
            Token::Number(n) => {
                let len = acc.len();
                acc[len - 1] += n;
                acc
            }
        });

    weights.sort();

    println!("Part 1 answer {}", *weights.iter().max().unwrap_or(&0));
    println!(
        "Part 2 answer {:?}",
        weights.iter().rev().take(3).sum::<i32>(),
    );
}
