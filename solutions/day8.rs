const SIZE: usize = 99;

type Table = [[i32; SIZE]; SIZE];

fn max_clamp(max: &mut i32, item: &i32) -> i32 {
    if item > max {
        *max = *item;
        return 1;
    }
    0
}

fn increment_visibilities(thresh: usize, height_visbilities: &mut [i32; 10]) {
    for (idx, val) in height_visbilities.iter_mut().enumerate() {
        if idx <= thresh {
            *val = 1;
        } else {
            *val += 1;
        }
    }
}

fn combine_tables(first: &Table, second: &Table, rotate_second: bool) -> Table {
    let mut output = [[0; SIZE]; SIZE];
    for rdx in 0..SIZE {
        for cdx in 0..SIZE {
            output[rdx][cdx] = match rotate_second {
                true => first[rdx][cdx] * second[cdx][rdx],
                false => first[rdx][cdx] * second[rdx][cdx],
            };
        }
    }

    output
}

fn calculate_visibility(table: &Table) -> Table {
    let mut f_visibility = [[0; SIZE]; SIZE];
    let mut b_visibility = [[0; SIZE]; SIZE];

    for (rdx, row) in table.iter().enumerate() {
        let (mut f_vis, mut b_vis) = ([0; 10], [0; 10]);
        let rev = row.iter().enumerate().rev();

        let cols = row.iter().enumerate().zip(rev);

        for ((f_cdx, &f_col), (b_cdx, &b_col)) in cols {
            f_visibility[rdx][f_cdx] = f_vis[f_col as usize];
            b_visibility[rdx][b_cdx] = b_vis[b_col as usize];

            increment_visibilities(f_col as usize, &mut f_vis);
            increment_visibilities(b_col as usize, &mut b_vis);
        }
    }

    combine_tables(&f_visibility, &b_visibility, false)
}

fn main() {
    let input = include_str!("../input/day8.txt");
    let mut table = [[[0; SIZE]; SIZE]; 2];

    for (ldx, line) in input.lines().enumerate() {
        for (cdx, character) in line.chars().enumerate() {
            let height = match character {
                x @ '0'..='9' => (x as i32) - 48,
                _ => panic!("Found non-integer char"),
            };

            table[0][ldx][cdx] = height;
            table[1][cdx][ldx] = height;
        }
    }

    // I was very tired when I wrote this, I forgot how it works
    // I am too lazy to go back and clean it up
    let mut part_1 = [[0; SIZE]; SIZE];
    for rdx in 0..SIZE {
        let (mut row_for, mut row_bak, mut col_for, mut col_bak) = (-1, -1, -1, -1);

        for cdx in 0..SIZE {
            part_1[rdx][cdx] |= max_clamp(&mut row_for, &table[0][rdx][cdx]);
            part_1[rdx][SIZE - 1 - cdx] |= max_clamp(&mut row_bak, &table[0][rdx][SIZE - 1 - cdx]);
            part_1[cdx][rdx] |= max_clamp(&mut col_for, &table[0][cdx][rdx]);
            part_1[SIZE - 1 - cdx][rdx] |= max_clamp(&mut col_bak, &table[0][SIZE - 1 - cdx][rdx]);
        }
    }

    let part_2 = combine_tables(
        &calculate_visibility(&table[0]),
        &calculate_visibility(&table[1]),
        true,
    );

    let part_1: i32 = part_1.iter().map(|row| row.iter()).flatten().sum();

    let part_2: i32 = part_2
        .iter()
        .map(|row| row.iter())
        .flatten()
        .max()
        .expect("wut")
        .clone();

    println!("Part 1: {part_1}");
    println!("Part 2: {part_2}");
}
