#[derive(Debug)]
struct Move(Direction, u32);

#[derive(Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Move {
    fn new(m: &str) -> Move {
        let mut pieces = m.split(" ");
        let direction = pieces
            .next()
            .map(|dir| match dir {
                "U" => Direction::Up,
                "D" => Direction::Down,
                "L" => Direction::Left,
                "R" => Direction::Right,
                _ => panic!("bad direction"),
            })
            .expect("no direction");

        let magnitude = pieces
            .next()
            .and_then(|mag| u32::from_str_radix(mag, 10).ok())
            .expect("no magnitude");

        Self(direction, magnitude)
    }
}

fn main() {
    let input = include_str!("../input/day9.txt");
    let moves = input
        .lines()
        .map(|line| Move::new(line))
        .collect::<Vec<Move>>();

    println!("{moves:?}");
}
