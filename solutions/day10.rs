#[derive(Debug)]
enum Instruction {
    NoOp,
    AddX(i32),
}

impl Instruction {
    fn from_str(s: &str) -> Self {
        let mut pieces = s.split(" ");
        let instruction = pieces.next().expect("no instruction");

        if instruction == "noop" {
            return Self::NoOp;
        }

        let param = pieces
            .next()
            .and_then(|num| i32::from_str_radix(num, 10).ok())
            .expect("no parameter");

        Self::AddX(param)
    }

    fn cycles(&self) -> i32 {
        match self {
            Self::NoOp => 1,
            Self::AddX(_) => 2,
        }
    }

    fn exec(&self, input: i32) -> i32 {
        match self {
            Self::NoOp => input,
            Self::AddX(num) => input + num,
        }
    }
}

fn strength(cycle: i32, reg: i32, cost: i32) -> i32 {
    match (cycle - 20i32).rem_euclid(40) {
        0 if cycle >= 19 => cycle * reg,
        39 if cycle >= 19 && cost == 2 => (cycle + 1) * reg,
        _ => 0,
    }
}

fn part_1(instructions: &Vec<Instruction>) -> i32 {
    let mut reg = 1;
    let mut cycle = 1;
    let mut strength_sum = 0;

    for instruction in instructions.iter() {
        let cost = instruction.cycles();

        strength_sum += strength(cycle, reg, cost);
        cycle += cost;
        reg = instruction.exec(reg)
    }

    strength_sum
}

fn part_2(instructions: &Vec<Instruction>) -> String {
    let mut screen = String::new();
    let mut reg = 1;
    let mut cycle = 0;

    for instruction in instructions.iter() {
        let cost = instruction.cycles();
        for tick in 1..=cost {
            let screen_pos = cycle % 40;
            if screen_pos == 0 {
                screen.push('\n');
            }

            if reg - 1 <= screen_pos && reg + 1 >= screen_pos {
                screen.push('#');
            } else {
                screen.push('.');
            }
            cycle += 1;
        }
        reg = instruction.exec(reg);
    }
    screen
}

fn main() {
    let input = include_str!("../input/day10.txt");
    let instructions = input
        .lines()
        .map(|line| Instruction::from_str(line))
        .collect::<Vec<_>>();

    println!("Part 1: {}", part_1(&instructions));
    println!("Part 2: {}", part_2(&instructions));
}
