use regex::Regex;

const MONKEY_REG: &'static str = r"^Monkey (\d{1}):";
const ITEMS_REG: &'static str = r"^  Starting items: ((\d+, )*(\d+))";
const OPERATION_REG: &'static str =
    r"^  Operation: new = (old|\d+) (\+|\*) (old|\d+)";
const TEST_REG: &'static str = r"^  Test: divisible by (\d+)";
const IF_REG: &'static str = r"^    If (true|false): throw to monkey (\d+)";

enum Operand {
    Num(i32),
    Old,
}

enum Operation {
    Add(Operand, Operand),
    Mul(Operand, Operand),
}

struct Test {
    modulus: i32,
    test_fn: Box<dyn Fn(bool) -> i32>,
}

struct Monkey {
    starting_items: Vec<i32>,
    operation: Operation,
    test: Test,
}

impl Operand {
    fn val(&self, old: &i32) -> i32 {
        if let &Self::Num(num) = self {
            return num;
        }
        *old
    }
}

impl Operation {
    fn exec(&self, old: &i32) -> i32 {
        match self {
            Self::Add(op1, op2) => op1.val(old) + op2.val(old),
            Self::Mul(op1, op2) => op1.val(old) * op2.val(old),
        }
    }
}

fn parse_monkeys(input: &str) -> Vec<Monkey> {
    let mut monkeys = Vec::new();
    let mut scratch = String::new();

    let monkey_reg = Regex::new(MONKEY_REG)
        .expect("Failed to parse mokey regex");
    let items_reg = Regex::new(ITEMS_REG)
        .expect("Failed to parse items regex");
    let operation_reg = Regex::new(OPERATION_REG)
        .expect("Failed to parse operation regex");
    let test_reg = Regex::new(TEST_REG)
        .expect("Failed to parse test regex");
    let if_reg = Regex::new(IF_REG)
        .expect("Failed to parse if regex");

    for line in input.lines() {

        if line == "" {
            for m in monkey_reg.captures_iter(&scratch) {
                println!("{:?}", m.get(1).unwrap().as_str());
            }

            scratch.clear();
            continue
        }

        scratch.push_str(line);
        scratch.push('\n');
    }

    monkeys
}

fn main() {
    let input = include_str!("../input/day11.txt");

    parse_monkeys(&input);
}
