#[repr(i32)]
enum Rps {
    Rock = 1,
    Paper = 2,
    Scissor = 3,
}

enum Outcome {
    Win,
    Lose,
    Draw,
}

impl Rps {
    fn from_char(s: char) -> Option<Self> {
        match s {
            'A' | 'X' => Some(Self::Rock),
            'B' | 'Y' => Some(Self::Paper),
            'C' | 'Z' => Some(Self::Scissor),
            _ => None,
        }
    }

    fn round(&self, other: &Self) -> i32 {
        match (self, other) {
            (Self::Rock, Self::Scissor)
            | (Self::Paper, Self::Rock)
            | (Self::Scissor, Self::Paper) => 6,
            (Self::Rock, Self::Rock)
            | (Self::Paper, Self::Paper)
            | (Self::Scissor, Self::Scissor) => 3,
            (Self::Rock, Self::Paper)
            | (Self::Scissor, Self::Rock)
            | (Self::Paper, Self::Scissor) => 0,
        }
    }
}

impl Outcome {
    fn from_char(c: char) -> Option<Self> {
        match c {
            'X' => Some(Self::Lose),
            'Y' => Some(Self::Draw),
            'Z' => Some(Self::Win),
            _ => None,
        }
    }

    fn rps(&self, opponent: &Rps) -> Rps {
        match (self, opponent) {
            (Self::Win, Rps::Scissor) | (Self::Lose, Rps::Paper) | (Self::Draw, Rps::Rock) => {
                Rps::Rock
            }
            (Self::Win, Rps::Rock) | (Self::Lose, Rps::Scissor) | (Self::Draw, Rps::Paper) => {
                Rps::Paper
            }
            (Self::Win, Rps::Paper) | (Self::Lose, Rps::Rock) | (Self::Draw, Rps::Scissor) => {
                Rps::Scissor
            }
        }
    }
}

fn main() {
    let score_1 = include_str!("../input/day2.txt")
        .lines()
        .filter_map(|line| {
            let mut chars = line.chars();
            Some((chars.nth(0)?, chars.nth(1)?))
        })
        .filter_map(|(t, u)| Some((Rps::from_char(t)?, Rps::from_char(u)?)))
        .fold(0, |acc, (them, us)| acc + us.round(&them) + (us as i32));

    let score_2 = include_str!("../input/day2.txt")
        .lines()
        .filter_map(|line| {
            let mut chars = line.chars();
            Some((chars.nth(0)?, chars.nth(1)?))
        })
        .filter_map(|(t, u)| Some((Rps::from_char(t)?, Outcome::from_char(u)?)))
        .map(|(t, u)| (u.rps(&t), t))
        .fold(0, |acc, (us, them)| acc + us.round(&them) + (us as i32));

    println!("Score part 1 {score_1}");
    println!("Score part 2 {score_2}");
}
