#![feature(iter_array_chunks)]

use std::collections::HashMap;

fn priority(c: char) -> i32 {
    match c {
        c @ 'a'..='z' => (c as i32) - 96,
        c @ 'A'..='Z' => (c as i32) - 65 + 27,
        _ => 0,
    }
}

fn find_common_char(strings: [&str; 3], map: &mut HashMap<char, i32>) -> char {
    strings
        .iter()
        .enumerate()
        .map(|(idx, string)| (idx as i32, string))
        .for_each(|(idx, string)| {
            string
                .chars()
                .for_each(|character| match map.get_mut(&character) {
                    Some(gen) => {
                        if *gen == idx - 1 {
                            *gen = idx;
                        }
                    }
                    None => {
                        if idx == 0 {
                            map.insert(character, idx);
                        }
                    }
                })
        });

    let (key, _) = map
        .iter()
        .filter(|(_, &v)| v == 2)
        .next()
        .expect("No characters have value of 3!");

    *key
}

fn main() {
    let input = include_str!("../input/day3.txt");
    let mut scratch_hash: HashMap<char, i32> = HashMap::new();
    scratch_hash.reserve(75);

    let part_1 = input
        .lines()
        .map(|line| (&line[0..(line.len() / 2)], &line[(line.len() / 2)..]))
        .fold(0, |acc, (first, second)| {
            first.chars().for_each(|c| {
                scratch_hash.insert(c, priority(c));
            });

            let priority = second
                .chars()
                .fold(0, |acc, ref c| match scratch_hash.get(c) {
                    Some(&priority) => priority,
                    None => acc + 0,
                });

            scratch_hash.clear();
            acc + priority
        });

    let part_2 = input.lines().array_chunks::<3>().fold(0, |acc, rucksacks| {
        let badge = find_common_char(rucksacks, &mut scratch_hash);
        let priority = priority(badge);

        scratch_hash.clear();
        acc + priority
    });

    println!("The answer for part 1 is {part_1}");
    println!("The answer for part 2 is {part_2}");
}
