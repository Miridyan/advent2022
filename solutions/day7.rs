use regex::Regex;
use std::collections::HashMap;

lazy_static::lazy_static! {
    pub static ref COMMAND: Regex =
        Regex::new(r"^(\$ ls|\$ cd|dir|\d+)( ([A-Za-z/.]+)|)")
            .expect("Failed to build check regex");
}

#[derive(Debug)]
enum Token<'a> {
    Ls,
    Cd(&'a str),
    Dir(&'a str),
    File(File),
}

#[derive(Debug)]
struct File {
    name: String,
    size: usize,
}

#[derive(Debug)]
struct Directory {
    children: HashMap<String, Directory>,
    files: Vec<File>,
}

struct Path {
    ctx: Vec<String>,
}

impl<'a> From<&'a str> for Token<'a> {
    fn from(other: &'a str) -> Token<'a> {
        let caps = COMMAND.captures(other).expect("No captures");

        match caps.get(1).expect("failed to get leader").as_str() {
            "$ ls" => Self::Ls,
            "$ cd" => {
                let dir = caps.get(3).expect("failed to get dir").as_str();

                Self::Cd(dir)
            }
            "dir" => {
                let dir = caps.get(3).expect("failed to get dir").as_str();

                Self::Dir(dir)
            }
            num @ _ => {
                let size = usize::from_str_radix(num, 10).expect("Failed to parse size");
                let name = caps.get(3).expect("failed to get dir").as_str();

                Self::File(File {
                    name: name.to_string(),
                    size,
                })
            }
        }
    }
}

impl Directory {
    fn new() -> Self {
        Self {
            children: HashMap::new(),
            files: Vec::new(),
        }
    }

    fn get_directory_mut(&mut self, key: &str) -> &mut Self {
        if !self.children.contains_key(key) {
            self.children.insert(key.to_string(), Self::new());
        }

        self.children.get_mut(key).expect("Shouldn't happen")
    }

    fn push_dir(mut self, directory: &Path) -> Self {
        directory
            .iter()
            .fold(&mut self, |acc, dir| acc.get_directory_mut(dir));

        self
    }

    fn push_file(mut self, directory: &Path, file: File) -> Self {
        let dest = directory
            .iter()
            .fold(&mut self, |acc, dir| acc.get_directory_mut(dir));

        dest.files.push(file);

        self
    }
}

impl Path {
    fn new() -> Self {
        Self { ctx: Vec::new() }
    }

    fn cd(mut self, dir: &str) -> Self {
        match dir {
            "/" => {
                self.ctx.clear();
            }
            ".." => {
                self.ctx.pop();
            }
            e @ _ => {
                self.ctx.push(e.to_string());
            }
        }

        self
    }

    fn is_root(&self) -> bool {
        self.ctx.len() == 0
    }

    fn iter(&self) -> impl Iterator<Item = &String> {
        self.ctx.iter()
    }
}

fn main() {
    let input = include_str!("../input/day7.txt");
    let (filesystem, _) = input.lines().map(Token::from).fold(
        (Directory::new(), Path::new()),
        |(fs, path), token| match token {
            Token::Ls => (fs, path),
            Token::Cd(dir) => (fs, path.cd(dir)),
            Token::File(f) => (fs.push_file(&path, f), path),
            Token::Dir(dir) => {
                let dir = path.cd(dir);

                (fs.push_dir(&dir), dir)
            }
        },
    );
}
